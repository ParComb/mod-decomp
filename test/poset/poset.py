# Poset (as dict) helpers

class Poset:
    def __init__(self, dico=None):
        self.predecessors = dict()
        if dico is not None:
            for (elem,nxts) in dico.items():
                self.add(elem, nxts)

    def __iter__(self):
        return iter(self.predecessors)

    def __len__(self):
        return len(self.predecessors)
    
    def copy(self):
        copy = Poset()
        for (k, v) in self.predecessors.items():
            copy.predecessors[k] = v

        return copy

    def deepcopy(self):
        copy = Poset()
        for (k, v) in self.predecessors.items():
            copy.predecessors[k] = { x for x in v }
        return copy

    def isempty(self):
        return len(self.elems()) == 0

    def elems(self):
        return self.predecessors.keys()

    def add(self, elem, predecessors=None):
        if predecessors is None:
            predecessors = set()
        nxts = set()
        if elem in self.predecessors:
            nxts = self.predecessors[elem]
        else:
            self.predecessors[elem] = nxts

        nxts.update(predecessors)

        for newelem in predecessors:
            if newelem not in self.predecessors:
                self.predecessors[newelem] = set()

        return self

    def remove(self, elem):
        to_links = self.predecessors[elem]
        for (v, elems) in self.predecessors.items():
            if elem in elems:
                elems.remove(elem)
                for u in to_links:
                    self.add_rel(u,v)

        del self.predecessors[elem]

    def remove_rel(self, less, more):
        less_preds = self.predecessors[less]

        more_preds = self.predecessors[more]
        if less not in more_preds:
            return None
        else:
            more_preds.remove(less)

        self.add(more, less_preds)

        return self

    def covering(self, elem):
        return self.successors(elem)

    def downset(self, elem):
        preds = {k for k in self.predecessors[elem]}

        changed = True
        while changed:
            changed = False
            for pred in preds:
                npreds = preds | self.downset(pred)
                if len(npreds) > len(preds):
                    preds = npreds
                    changed = True

        return preds

    def successors(self, elem):
        succs = set()
        for candidate in self.predecessors.keys():
            if elem in self.predecessors[candidate]:
                succs.add(candidate)

        return succs

    def less_than(self, elem1, elem2):
        if elem1 in self.predecessors[elem2]:
            return True

        for pelem2 in self.predecessors[elem2]:
            if self.less_than(elem1, pelem2):
                return True

        return False

    def transitive_reduction(self):
        poset = self.deepcopy()
        for (elem, preds) in self.predecessors.items():
            for pred in preds:
                poset.predecessors[elem].remove(pred)
                if not poset.less_than(pred, elem):
                    # put it back if we loose the (transitive) relation
                    poset.predecessors[elem].add(pred)
        self.predecessors = poset.predecessors

    def add_rel(self, elem1, elem2):
        if self.less_than(elem1, elem2):
            # do nothing
            return None
        self.predecessors[elem2].add(elem1)

    def upset(self, elem):
        succs = set()

        for candidate in self.predecessors.keys():
            if elem in self.predecessors[candidate]:
                succs.add(candidate)

        changed = True
        while changed:
            changed = False
            for succ in succs:
                nsuccs = succs | self.upset(succ)
                if len(nsuccs) > len(succs):
                    changed = True
                    succs = nsuccs

        return succs

    def ismaximal(self, elem):
        return not self.successors(elem)

    def maximal_elements(self):
        return { elem for elem in self.predecessors.keys() if self.ismaximal(elem)}

    def all_predecessors(self, elems):
        allpreds = set()
        for elem in elems:
            allpreds.update(self.predecessors[elem])
        return allpreds

    def __str__(self):
        rels = []
        for elem in self.predecessors:
            for nxt in self.predecessors[elem]:
                rels.append((elem, nxt))
        return "({}, {})".format({k for k in self.predecessors}
                                 , ", ".join(("{}>{}".format(elem,nxt) for (elem, nxt) in rels)))
    def to_dot(self):
        dot = """
graph {
  layout = dot
  rankdir = TB
  node [ shape="none" ]
"""
        dot += "\n  /* elements */\n"
        dot += "\n".join(('  {} [label="{}"];'.format(elem, elem) for elem in self.predecessors))

        dot += "\n  /* relations */\n"
        for (elem, preds) in self.predecessors.items():
            dot += "\n".join(("  {} -- {};".format(elem, pred) for pred in preds))

        dot +="\n}\n"
        return dot

    def to_dict(self):
        dico = {}
        for elem in self.predecessors:
            dico[elem] = []  # use lists because it is required in Sage
            for nxt in self.predecessors[elem]:
                dico[elem].append(nxt)
        return dico

    def all_successors(self):
        return {u : self.successors(u) for u in self}


def transitive_closure(poset):
    # compute the transitive closure from a poset
    # the result is a dictionary  (adjascency list)
    trans = { src : set() for src in poset.predecessors }
    stk = [(n, npreds) for (n, npreds) in poset.predecessors.items() if not poset.successors(n)]
    reach = { n:{n} for n, _ in stk }    
    while stk:
        n, npreds = stk.pop()
        for npred in npreds:
            npred_succs = trans.get(npred, set())
            npred_succs.update(reach[n])
            trans[npred] = npred_succs
            npred_reach = reach.get(npred, set())
            npred_reach.update(reach[n])
            npred_reach.add(npred)
            reach[npred] = npred_reach
            stk.append((npred, poset.predecessors[npred]))

    clos = dict()
    for n, ms in trans.items():
        if n not in clos:
            clos[n] = set()
        for m in ms:
            if m in clos:
                clos[m].add(n)
            else:
                clos[m] = {n}
    return clos
    # return {n:list(ms) for n, ms in trans.items()}

Poset.transitive_closure = transitive_closure

    
def from_dict(dico):
    p = Poset()
    for elem, preds in dico.items():
        p.predecessors[elem] = set(preds)
        for pred in preds:
            if pred not in p.predecessors:
                p.predecessors[pred] = set()
    return p

def poset_to_svg(poset, prefix="images/poset", dot_path='dot'):
    def find_tempfilename(prefix=prefix+"_", suffix=".dot"):
        for i in range(99999):
            filename = "{}{}{}".format(prefix, i, suffix)
            try:
                f = open(filename, 'r')
                f.close()
            except OSError:
                return filename

    dot_filename = find_tempfilename()

    with open(dot_filename, 'w') as dot_file:
        dot_file.write(poset.to_dot())

    svg_filename = dot_filename + '.svg'

    import subprocess
    ret_code = subprocess.check_call("{dot_path} -Tsvg {dot_filename} -o {svg_filename}".format(**locals()), shell=True)

    svg_ret = ""
    with open(svg_filename, 'r') as svg_file:
            svg_ret = svg_file.read()

    #import os
    #os.remove(dot_filename)
    #os.remove(svg_filename)

    return svg_ret

def show_poset(poset):
    import IPython.display
    return IPython.display.SVG(poset_to_svg(poset))

Poset.show = show_poset



import sys
sys.path.append("../../src")

import moddecomp as md
from twostructure import twostructure_from_dict, TwoStructure

import poset

def ts_from_dict(dico):
    po = poset.from_dict(dico)
    return twostructure_from_dict(poset.transitive_closure(po))

def series_1():
    return ts_from_dict({"a" : {"b"}, "b" : {"c"}, "c" : {"d"}, "d" : set()})

def incomps_1():
    return ts_from_dict({"a": {}, "b" : {}, "c" : {}, "d" : {}})

def prime_1():
    # the minimal prime poset, a.k.a, the N-poset
    return ts_from_dict({"a" : {"c", "d"}, "b" : {"d"}, "c" : set(), "d" : set()})

def tree_1():
    return ts_from_dict({"a" : {"b", "c"}, "b" : {"d" , "e"}, "c" : set(), "d" : set(), "e" : set()})

def noroot():
    ts = TwoStructure()
    ts.edge("a", "b", 1)
    ts.edge("b", "a", 1)
    ts.edge("c", "d", 1)
    ts.edge("d", "c", 1)
    ts.edge("a", "c", 1)
    ts.edge("a", "d", 1)
    ts.edge("b", "c", 1)
    ts.edge("b", "d", 1)
    return ts


if __name__ == "__main__":

    po1 = series_1()
    #print(po1.to_dict())
 
    dtree1 = md.modular_decomposition(po1)
    print(dtree1.to_decomp())

    po2 = incomps_1()
    dtree2 = md.modular_decomposition(po2)
    #print(dtree2.to_decomp())

    po3 = prime_1()
    dtree3 = md.modular_decomposition(po3)
    #print(dtree3.to_decomp())

    po4 = tree_1()
    dtree4 = md.modular_decomposition(po4)
    #print(dtree4.to_decomp())

    nr = noroot()
    nrtree = md.modular_decomposition(nr)
    print(nrtree.to_decomp())
